import pandas as pd
from pm4py.objects.conversion.log import converter as log_converter
from pm4py.visualization.petrinet import visualizer as pn_visualizer
from pm4py.visualization.dfg import visualizer as dfg_visualization
from pm4py.algo.discovery.dfg import algorithm as dfg_discovery
import glob, os

nombre = 'vacio'
ruta = pd.DataFrame(columns=('concept:name', 'case:concept:name', 'time:timestamp  time'))


def limpiar(data):
    data['time'] = pd.to_datetime(data['time'])
    # seleccionar solo los eventos que pasan delante de fecha inicio MOOC
    data = data.loc[data['time'] > '2019-04-15']
    data2 = data[['event_type','context','time']]
    dt2 = pd.io.json.json_normalize(data['context'])
    data2['user_id'] = dt2['user_id'].values
    data2 = data2.replace(['hide_transcript', 'load_video', 'pause_video', 'play_video', 'seek_video', 'show_transcript',
                         'speed_change_video', 'stop_video', 'video_hide_cc_menu', 'video_show_cc_menu'], 'video')
    data2 = data2.replace(['edx.forum.comment.created', 'edx.forum.response.created', 'edx.forum.response.voted',
                         'edx.forum.searched', 'edx.forum.thread.created', 'edx.forum.thread.voted'], 'forum')
    data2 = data2.replace(['openassessmentblock.get_peer_submission',
                         'openassessmentblock.get_submission_for_staff_grading', 'openassessmentblock.peer_assess',
                         'openassessmentblock.self_assess', 'openassessmentblock.staff_assess',
                         'openassessmentblock.submit_feedback_on_assessments',
                         'openassessmentblock.create_submission', 'openassessmentblock.save_submission',
                         'openassessment.student_training_assess_example', 'openassessment.upload_file'], 'test')
    data2 = data2.replace(['page_close', 'seq_goto', 'seq_next', 'seq_prev'], 'interaction')
    data2 = data2.replace(['edx.course.enrollment.activated', 'edx.course.enrollment.deactivated',
                         'edx.course.enrollment.mode_changed', 'edx.course.enrollment.upgrade.clicked',
                         'edx.course.enrollment.upgrade.succeeded'], 'instriccion')
    data2 = data2.replace(['book', 'textbook.pdf.thumbnails.toggled', 'textbook.pdf.thumbnail.navigated',
                         'textbook.pdf.outline.toggled', 'textbook.pdf.chapter.navigated',
                         'textbook.pdf.page.navigated', 'textbook.pdf.zoom.buttons.changed',
                         'textbook.pdf.zoom.menu.changed', 'textbook.pdf.display.scaled',
                         'textbook.pdf.page.scrolled', 'textbook.pdf.search.executed',
                         'textbook.pdf.search.navigatednext', 'textbook.pdf.search.highlight.toggled',
                         'textbook.pdf.searchcasesensitivity.toggled'], 'text_book')
    data2 = data2.replace(['edx.problem.hint.demandhint_displayed', 'edx.problem.hint.feedback_displayed',
                         'problem_check', 'problem_check_fail', 'problem_graded', 'problem_rescore',
                         'problem_rescore_fail', 'problem_reset', 'problem_save', 'problem_show', 'reset_problem',
                         'reset_problem_fail', 'save_problem_fail', 'save_problem_success', 'showanswer'], 'problem')
    data2 = data2.loc[data2['event_type'].isin(['video', 'forum', 'test', 'interaction', 'instriccion', 'text_book', 'problem'])]
    ruta = data2[['event_type','user_id','time']]
    ruta = ruta.rename({'event_type': 'concept:name', 'user_id': 'case:concept:name', 'time': 'time:timestamp'}, axis=1)
    grafo_ruta(ruta,'general')
    # fecha se transforma al numero de la semana año
    data['time'] = pd.to_datetime(data['time']).dt.week
    ruta['time']=pd.to_datetime(ruta['time:timestamp']).dt.week
    # calcula las 6 semanas del curso MOOC y selecciona
    lim = list(data['time'].unique())[0] + 6
    # calcular el numero limite para restar y calcular semana MOOC
    lim2 = list(data['time'].unique())[0] - 1
    # elige solo las fechas dentro de las semanas MOOC
    data = data.loc[data['time'] < lim]
    ruta = ruta.loc[ruta['time'] < lim]
    # al numero de semana año la pasa a semana MOOC
    data['time'] = data['time'] - lim2
    ruta['time'] = ruta['time'] - lim2
    sema1 = ruta.loc[ruta['time']==1]
    sema2 = ruta.loc[ruta['time'] == 2]
    sema3 = ruta.loc[ruta['time'] == 3]
    sema4 = ruta.loc[ruta['time'] == 4]
    sema5 = ruta.loc[ruta['time'] == 5]
    sema6 = ruta.loc[ruta['time'] == 6]
    try:
        for i in range(1,7):
            os.remove('recomendar/static/img/sema',i,'.png')
    except:
        print('')
    grafo_ruta(sema1, 'sema1')
    grafo_ruta(sema2, 'sema2')
    grafo_ruta(sema3, 'sema3')
    grafo_ruta(sema4, 'sema4')
    grafo_ruta(sema5, 'sema5')
    grafo_ruta(sema6, 'sema6')

    # reemplaza los eventos en el numero del evento general que pertence
    data = data.replace(['hide_transcript', 'load_video', 'pause_video', 'play_video', 'seek_video', 'show_transcript',
                         'speed_change_video', 'stop_video', 'video_hide_cc_menu', 'video_show_cc_menu'], 1)
    data = data.replace(['edx.forum.comment.created', 'edx.forum.response.created', 'edx.forum.response.voted',
                         'edx.forum.searched', 'edx.forum.thread.created', 'edx.forum.thread.voted'], 2)
    data = data.replace(['openassessmentblock.get_peer_submission',
                         'openassessmentblock.get_submission_for_staff_grading', 'openassessmentblock.peer_assess',
                         'openassessmentblock.self_assess', 'openassessmentblock.staff_assess',
                         'openassessmentblock.submit_feedback_on_assessments',
                         'openassessmentblock.create_submission', 'openassessmentblock.save_submission',
                         'openassessment.student_training_assess_example', 'openassessment.upload_file'], 3)
    data = data.replace(['page_close', 'seq_goto', 'seq_next', 'seq_prev'], 4)
    data = data.replace(['edx.course.enrollment.activated', 'edx.course.enrollment.deactivated',
                         'edx.course.enrollment.mode_changed', 'edx.course.enrollment.upgrade.clicked',
                         'edx.course.enrollment.upgrade.succeeded'], 5)
    data = data.replace(['book', 'textbook.pdf.thumbnails.toggled', 'textbook.pdf.thumbnail.navigated',
                         'textbook.pdf.outline.toggled', 'textbook.pdf.chapter.navigated',
                         'textbook.pdf.page.navigated', 'textbook.pdf.zoom.buttons.changed',
                         'textbook.pdf.zoom.menu.changed', 'textbook.pdf.display.scaled',
                         'textbook.pdf.page.scrolled', 'textbook.pdf.search.executed',
                         'textbook.pdf.search.navigatednext', 'textbook.pdf.search.highlight.toggled',
                         'textbook.pdf.searchcasesensitivity.toggled'], 6)
    data = data.replace(['edx.problem.hint.demandhint_displayed', 'edx.problem.hint.feedback_displayed',
                         'problem_check', 'problem_check_fail', 'problem_graded', 'problem_rescore',
                         'problem_rescore_fail', 'problem_reset', 'problem_save', 'problem_show', 'reset_problem',
                         'reset_problem_fail', 'save_problem_fail', 'save_problem_success', 'showanswer'], 7)
    # selecciona solo los eventos que en realidad se soncideran eventos para MOOOC
    data = data.loc[data['event_type'].isin([1, 2, 3, 4, 5, 6, 7])]
    # elimina valores nulos de la columna time
    data = data.dropna(subset=['time'])
    # la columna context existe un json tomando ese valor
    dt = pd.io.json.json_normalize(data['context'])
    # del json anterior se toma solo el user_id y se le agrega al dataframe que se va a exportar
    data['user_id'] = dt['user_id'].values
    # elimina campos nulos de la columna user_id
    data = data[data['username'].map(len) > 0]
    # exporta dataframe
    #data.to_csv('eventMOOC.csv', sep='|')
    return data

def grafo_ruta (ruta,nombre):
    log = log_converter.apply(ruta)
    dfg = dfg_discovery.apply(log)
    parameters = {pn_visualizer.Variants.FREQUENCY.value.Parameters.FORMAT: "png"}
    gviz = dfg_visualization.apply(dfg, log=log, variant=dfg_visualization.Variants.FREQUENCY, parameters=parameters)
    archivo = 'recomendar/static/img/'+nombre+'.png'
    pn_visualizer.save(gviz, archivo)