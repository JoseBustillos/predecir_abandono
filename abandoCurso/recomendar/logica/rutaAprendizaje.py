#########################################################################################################################################
#
#							metodo para elaborar ruta de aprendizaje
#
#########################################################################################################################################
from recomendar.models import Usuario
import pandas as pd

def matriz(valor, valore, archivo):
    # lista para los cursos
    # lista de eventos
    columnas = ['Evento_video', 'Evento_foro', 'Evento_prueba_abierta', 'Evento_interaccion', 'Evento_instriccion',
                'Evento_textbook', 'Evento_problema', 'Nota']
    # filtrar datos unicos de cursos y usuarios por id_user
    cursoUni = valor.values('event_type').distinct()[:]
    nombreUni = valore.values('user').distinct()[:]
    # ciclo para recorrer las semanas de los MOOCs
    for i in range(1, 7):
        lista = []
        # lista prar los usuari∫os
        index = []
        # ciclo para recorrer los usuarios
        for v in nombreUni:
            # inicializar contador despues de cada usuario por los n cursos
            contar = []
            # buscar nota del usuario port medio del id y almacenar en una lista
            nota = list(Usuario.objects.filter(user_id=v['user']).values('status', 'user_id'))
            # condicion para saber si el usuario se encuentra registrado en las dos tablas
            if (len(nota) > 0):
                # obtener el diccionario que se encuentra en la lista anterior
                valorw = nota[0]
                # obteniendo la nota del usuario
                notas = valorw["status"]
                # obtener usuarios registrado
                usuarios = valorw["user_id"]
                # almacenar en una lista parar el index del csv
                index.append(usuarios)
                # cursos
                va = []
                # ciclo para recorrer los eventos
                for va in range(1, 8):
                    # proceso para contar un usuarios que a ingresado a varios cursos
                    contar.append(valore.filter(user=v['user'], event_type=va, time=i).count())
                    #print('user: ', v['user'], ' evento: ', va, 'semana: ', i, ' contar: ', contar)
                # agregar la nota a cada usuario para almacenar el csv
                contar.append(notas)
                lista.append(dict(zip(columnas, contar)))
        # convertir la lista en un dataframe con pandas
        dg = pd.DataFrame(lista, index=index)
        # almacenar el dataframe en archivo
        nombre = archivo + str(i) + '.csv'
        dg.to_csv(nombre)

    '''print("TOTAL CURSOS UNICOS: " + str(len(cursoUni)))
    print("TOTAL CURSOS : " + str(len(valor.values('type_event'))))
    print("TOTAL USUARIOS UNICOS: " + str(len(nombreUni)))
    print("TOTAL USUARIOS : " + str(len(valor.values('id_user'))))'''
