from sklearn.model_selection import train_test_split
# librerias sklearn para MLP
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn import tree
from sklearn.model_selection import GridSearchCV
from joblib import dump, load

columnas = ['Evento_foro','Evento_interaccion', 'Evento_problema', 'Evento_prueba_abierta', 'Evento_video', 'Nota']

#	perceptron multicapa
def mlp(data, valoru, valormax, capas):
    X = data[columnas[:-1]].values
    y = data['Nota'].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)
    mlp = MLPClassifier()
    param_grid = [{'alpha': [0.01, 0.001, 0.0001, 0.00001, 0.000001, 0.0000001],
                   'solver': ["lbfgs", "sgd", "adam"],
                   'activation': ["identity", "logistic", "tanh", "relu"],
                   'learning_rate': ["constant", "invscaling", "adaptive"],
                   'max_iter': [valormax],
                   'hidden_layer_sizes': [(valoru, valoru), (valoru,), (valoru, valoru,valoru)
                                          , (valoru, valoru,valoru,valoru)]}]
    gs = GridSearchCV(estimator=mlp, param_grid=param_grid, scoring='accuracy', cv=10, n_jobs=-1, verbose=0)
    gs.fit(X_train, y_train)
    configRed = gs.best_estimator_.get_params()
    # entrenar el modelo
    gs = gs.fit(X_train, y_train)
    # persistencia del modelo entenado
    dump(gs, 'estudianteMLP.joblib')
    # predecir valores para las metricas
    y_pred = gs.predict(X_test)
    accuracy, recall, precision = metrics(y_test, y_pred)
    return accuracy, recall, precision


#	arbol de decision
def arbol(data, valormax):
    print(type(data))
    X = data[columnas[:-1]].values
    y = data['Nota'].values
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)
    # usar arbol de decision
    ppn = tree.DecisionTreeClassifier(max_depth=valormax)
    # entrenar el modelo
    ppn = ppn.fit(X_train, y_train)
    # persistencia del modelo entenado
    dump(ppn, 'estudianteARBOL.joblib')
    # predecir valores
    y_pred = ppn.predict(X_test)
    accuracy, recall, precision = metrics(y_test, y_pred)
    return accuracy, recall, precision


#	metricas
def metrics(y_test, y_pred):
    return (accuracy_score(y_test, y_pred), recall_score(y_test, y_pred, average='macro'),
            precision_score(y_test, y_pred, average='macro'))

def predModel(valor, data, data2,slug):
    # leer archivo donde se encuentran los datos que vamos a predecir
    prediccion = []
    #  cargar modelo pre entrenado
    if(slug=='mlp'):
        clf = load('estudianteMLP.joblib')
    else:
        clf = load('estudianteARBOL.joblib')
    # predecir los eventos
    for i in range(0, len(valor)):
        prediccion.append(clf.predict([valor[i]])[0])
    data['estado'] = prediccion
    data['user_id'] = data2['user_id']
    # reordenar las columnas para que las predicciones sean correctas
    data = data[['user_id', '1', '2', '3', '4', '7', 'estado']]
    return data
