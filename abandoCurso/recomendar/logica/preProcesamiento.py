#########################################################################################################################################
#
#							metodo para leer archivo
#
#########################################################################################################################################
import pandas as pd

def limpiezaIA(listaNombres, a):
    val = []
    # obtener las direcciones de los archivos creados de la ruta de
    # aprendizaje por semanas
    for ii in range(1, 7):
        # crear nombre de los archivos Efhe11v1 para almacenarlos en una lista
        val.append(''.join((listaNombres[0], a, str(ii), '.csv')))
    # leer primer archivo
    newDF = pd.read_csv(val[0])
    n = pd.read_csv(val[0])
    # ciclo para leer los demas archivos para unificarlos
    for iii in range(1, len(val)):
        # crean un nuevo dataframe unificando los datos de los archivos csv
        newDF = newDF.add(
            pd.read_csv(val[iii])[['Evento_video', 'Evento_foro',
                                   'Evento_prueba_abierta','Evento_interaccion',
                                   'Evento_instriccion', 'Evento_textbook',
                                   'Evento_problema', 'Nota']])

    # unificar solo las 3 rimeras semanas
    newDF2 = pd.read_csv(val[0])
    newDF2 = newDF2.add(
        pd.read_csv(val[1])[['Evento_video', 'Evento_foro',
                             'Evento_prueba_abierta', 'Evento_interaccion',
                             'Evento_instriccion', 'Evento_textbook',
                             'Evento_problema', 'Nota']])
    newDF2 = newDF2.add(
        pd.read_csv(val[2])[['Evento_video', 'Evento_foro',
                             'Evento_prueba_abierta', 'Evento_interaccion',
                             'Evento_instriccion', 'Evento_textbook',
                             'Evento_problema', 'Nota']])

    newDF2['suma'] = newDF2.iloc[:, :].sum(axis=1)
    # lista de los indices que no interaccion 3 semanas
    reg1 = list(newDF2.loc[newDF2['suma'] == 0].index)
    reg2 = list(newDF2.loc[newDF2['Evento_video'] < 3].index)
    reg = reg1+reg2
    index2 = list(set(reg))
    nuevo = []
    # asignado el abandono
    for ii in range(0,len(index2)):
        nuevo.append('dropout')
    # columnas faltantes
    n['Nota'].update(pd.Series(nuevo, index = index2))
    newDF['Nota'] = n['Nota']
    newDF['Unnamed: 0'] = n['Unnamed: 0']
    # eliminar columna
    s = newDF.drop(['Unnamed: 0'], axis=1)
    # eliminar eventos no necesarios
    dato2 = pd.DataFrame(s.to_dict('records')).drop(columns=['Evento_textbook', 'Evento_instriccion'])
    # obtener todos los elementos segun el filtro
    downloadable = dato2.loc[dato2['Nota'] == 'downloadable']
    dropout =dato2.loc[dato2['Nota'] == 'dropout']
    notpassing = dato2.loc[dato2['Nota'] == 'notpassing']
    # creando lista vacio de tamaños de listas anteriores
    nume = [0,0,0]
    # obtener tamaño de las listas y asignar en lista creada
    nume[0]=len(downloadable)
    nume[1]=len(dropout)
    nume[2]=len(notpassing)
    # obtenemos el primer numeros de la lista
    menor = nume[0]
    # ciclo for para recorrer lista
    for numero in nume:
        # condicion para identificar numero menor
        if numero < menor:
            menor = numero
    # data con datos nivelados para evitar problemas en entrenar modelos
    dato3=pd.concat([downloadable.tail(menor),dropout.tail(menor),notpassing.tail(menor)])
    return dato3

def archivoPredict():
    data = pd.read_csv('datosPredecir.csv')
    # existe archivos sin evento problema = 3
    if '3' in data:
        # cambiamos el orden de los log con el orden de los datos que usamos para
        # el entrenamiento del modelo
        data = data[['user_id','2','4','7','3','1']]
    else:
        # si no existe evento 3 crea columna del evento 3 pero con valor 0
        data['3'] = 0
        data = data[['user_id', '2', '4', '7','3', '1']]
    data2 = data
    # elimnar columna de usuario innesesario
    data = data.drop('user_id', 1)
    # lista de nombres cabecera
    cabecera = list(data.columns)
    for ii in range(1, 5):
        valor = str(ii) not in cabecera
        if valor == True:
            # añade eventos que no han realizado los estudiantes para una mejor prediccion
            data[str(ii)] = 0
    # convertir dataframe en lista de listas
    valor = data.values.tolist()
    return valor, data, data2
