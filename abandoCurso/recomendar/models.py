# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Efhe11V1(models.Model):
    username = models.CharField(max_length=300, blank=True, null=True)
    event_source = models.CharField(max_length=30, blank=True, null=True)
    name = models.CharField(max_length=30, blank=True, null=True)
    accept_language = models.CharField(max_length=200, blank=True, null=True)
    time = models.IntegerField(blank=True, null=True)
    agent = models.CharField(max_length=300, blank=True, null=True)
    page = models.CharField(max_length=300, blank=True, null=True)
    host = models.CharField(max_length=300, blank=True, null=True)
    session = models.CharField(max_length=300, blank=True, null=True)
    referer = models.CharField(max_length=300, blank=True, null=True)
    context = models.CharField(max_length=600, blank=True, null=True)
    ip = models.CharField(max_length=30, blank=True, null=True)
    event = models.TextField(blank=True, null=True)
    event_type = models.CharField(max_length=300, blank=True, null=True)
    user = models.ForeignKey('Usuario', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'efhe11v1'


class Usuario(models.Model):
    user_id = models.IntegerField(primary_key=True)
    grade = models.FloatField(blank=True, null=True)
    course_id = models.CharField(max_length=300, blank=True, null=True)
    status = models.CharField(max_length=300, blank=True, null=True)
    name = models.CharField(max_length=400, blank=True, null=True)
    mode = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usuario'
