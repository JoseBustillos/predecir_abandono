from django.shortcuts import render
import pandas as pd
import numpy as np
from .forms import NewArticleForm2
from .forms import perceptronForm
from .forms import arbolForm
from .forms import NewArticleForm
from .forms import NameForm
from .forms import OptimaForm
from .models import Efhe11V1
from django.contrib import messages
from .logica.rutaAprendizaje import matriz
from .logica.machineLearn import mlp
from .logica.machineLearn import arbol
from .logica.machineLearn import predModel
from .logica.preProcesamiento import limpiezaIA
from .logica.preProcesamiento import archivoPredict
from .logica.limpiarData import limpiar
from collections import OrderedDict
from glob import glob
import json
import os

a = "_matriz"
listaNombres = ["Efhe11v1"]
# para mantener el orden de los eventos
a_dict = OrderedDict()
a_dict['user_id'] = 1
a_dict['Video'] = 2
a_dict['Forum'] = 3
a_dict['Test'] = 4
a_dict['Interaction'] = 5
#a_dict['Instriccion'] = 6
#a_dict['Textbook'] = 7
a_dict['Problem'] = 6

def rutaAprendizaje(request):
    valor = Efhe11V1.objects.order_by('event_type')
    valore = Efhe11V1.objects.order_by('user')
    # llamar al metodo para crear matriz
    matriz(valor, valore, listaNombres[0] + a)
    return render(request, 'abandoCurso/base.html')


def perceptronHtml(request):
    accuracy = 0
    recall = 0
    precision = 0
    if request.method == "POST":
        try:
            # llamar el formulario de elegir el curso
            form = NewArticleForm2(request.POST)
            form2 = perceptronForm(request.POST)
            # formulario para subir el archivo csv
            if form.is_valid():
                # obtener seleccion del curso del formulario
                # llamar metodo para leer archivo y visisualizar en el template
                # este metodo retorna varias variables por lo que
                # cada retorno se almacena en las varibles en el orden que esta
                dato2 = limpiezaIA(listaNombres, a)
                if form2.is_valid():
                    # obtener seleccion del curso del formulario
                    valoru = form2.cleaned_data['numero_neuronas']
                    valormax = form2.cleaned_data['maximas_interacciones']
                    capas = form2.cleaned_data['numero_capas']
                    if valoru is not None:
                        if valormax is not None:
                            accuracy, recall, precision = mlp(dato2, valoru, valormax,capas)
        except Exception as e:
            print(e)
    else:
        form2 = perceptronForm()
        form = NewArticleForm2()
    return render(request, 'abandoCurso/perceptron.html',
                  {'form': form, 'form2': form2, 'accuracy': accuracy, 'recall': recall,
                   'precision': precision})


def arbolHtml(request):
    # lista para guardar los datos de cursos y estudiante
    accuracy = 0
    recall = 0
    precision = 0
    if request.method == "POST":
        try:
            # llamar el formulario de elegir el curso
            form = NewArticleForm2(request.POST)
            form2 = arbolForm(request.POST)
            # formulario para subir el archivo csv
            dato2 = limpiezaIA(listaNombres, a)
            if form.is_valid():
                if form2.is_valid():
                    # obtener seleccion del curso del formulario
                    valormax = form2.cleaned_data['profundidad_maxima']
                    if valormax is not None:
                        accuracy, recall, precision = arbol(dato2, valormax)
        except Exception as e:
            print(e)
    else:
        form2 = arbolForm()
        form = NewArticleForm2()
    return render(request, 'abandoCurso/arbol.html',
                  {'form': form, 'form2': form2, 'accuracy': accuracy, 'recall': recall,
                   'precision': precision})

def index(request,slug):
    print(slug)
    a_dict['Status'] = 7
    datos = []
    l =[0,0,0]
    dictionary={}
    if request.method == "POST":
        try:
            csv_file = request.FILES["csv_file"]
            # let's check if it is a csv file
            if not csv_file.name.endswith('.csv'):
                messages.error(request, 'THIS IS NOT A CSV FILE')
            data_set = csv_file.read().decode('UTF-8')
            mostrar = True
            lines = data_set.split("\n")
            dato = []
            # leer las lineas del csv
            for i in range(0, len(lines) - 1):
                # crear listas con el separador
                # dato.append(lines[i].split("|"))
                dato.append(json.loads(lines[i]))
            # data por limpiar
            sucia = pd.DataFrame(dato)
            # metodo para limpiar data
            frame = limpiar(sucia)
            #################################################
            # preparar los datos para predecir el abandono del curso
            pred = frame.groupby('user_id')['event_type'].value_counts().unstack().fillna(0)
            # importar lo datos MOOCs que se va a predecir el abandono
            pred.to_csv('datosPredecir.csv')
            valor, data, data2 = archivoPredict()
            dato = predModel(valor, data, data2,slug)
            l = dato.groupby('estado').count()['1'].to_list()
            datos = dato.to_dict('records')
            for iiii in range(2,7):
                var = os.path.exists("semana" + str(iiii) + '.csv')
                if(var==True):
                    os.remove("semana" + str(iiii) + '.csv')
            for iii in range(1, len(datos)+1):
                # localizar las actividades por semana
                semana = frame.loc[frame['time'] == iii]
                # realizar un conteo por semana y estudiante
                semana = semana.groupby('user_id')['event_type'].value_counts().unstack().fillna(0)
                # registrar los log por semana en archivos csv
                semana.to_csv("semana" + str(iii) + '.csv')
        except Exception as e:
            print(e)
    if(len(l)==0):
        l.append(0)
        l.append(0)
        l.append(0)
    elif (len(l) == 1):
        l.append(0)
        l.append(0)
    else:
        l.append(0)
    return render(request, 'abandoCurso/index2.html',{'cabecera': a_dict, 'cursos':datos,
                                                      'aban':l[1],'pasa':l[0], 'rep':l[2],
                                                      'dict':dictionary,
                                                      'slug':slug, 'modelo':slug})


def semana2(request):
    val = {}
    if request.method == 'POST':
        form = NewArticleForm(request.POST)
        form2 = NameForm(request.POST)
        if form2.is_valid() and form.is_valid():
            # obtner el usuario que se desea saber su cantidad de eventos
            user = form2.cleaned_data['Seleccionar_Usuario']
            # leer varios archivos csv que empiezan con semana
            archivos = glob("semana*.csv")
            df2 = pd.DataFrame()
            # leer datos predecidos modelo
            ds2 = pd.read_csv('modeloResult.csv')
            # pbtner el estado nota
            s = ds2.loc[ds2['user_id'].isin([user])][['estado']]
            estado = s['estado'].values[0]
            # leer todos los archivos eventos semana csv
            ds = [pd.read_csv(f) for f in archivos]
            for i in range(0, len(ds)):
                valor = ds[i].loc[ds[i]['user_id'].isin([user])]
                if len(valor) > 0:
                    df2 = pd.concat([df2, valor])
                else:
                    d = {'user_id': [user], '1': [0], '2': [0], '3': [0], '4': [0], '5': [0], '6': [0], '7': [0]}
                    valor = pd.DataFrame(data=d)
                    df2 = pd.concat([df2, valor])
            nuevo = []
            for i in range(0, 6):
                nuevo.append(estado)
            df2['estado'] = nuevo
            val = df2.to_dict('records')
    else:
        form = NewArticleForm()
        form2 = NameForm()
    return render(request, 'abandoCurso/semana.html',
                  {'form': form, 'form2': form2, 'cursos': val, 'cabecera': a_dict})


def optima(request):
    data = pd.read_csv('modeloResult.csv')
    data = data.loc[data['estado'] == 'downloadable']
    val = []
    data["suma"] = data.iloc[:, 2:].sum(axis=1)
    data2 = data[data.suma == data.suma.max()]
    user = data2['user_id']
    if request.method == 'POST':
        form = OptimaForm(request.POST)
        if form.is_valid():
            select = form.cleaned_data['Seleccionar']
            val = semana(data2, user, select)
    else:
        form = OptimaForm()
    return render(request, 'abandoCurso/optima.html', {'form': form, 'cursos': val, 'cabecera': a_dict})


def promedio(request):
    a_dict['Suma'] = 8
    valSem = [0, 0, 0, 0, 0, 0]
    mostrar = True
    if request.method == "POST":
        try:
            csv_file = request.FILES["csv_file"]
            # let's check if it is a csv file
            if not csv_file.name.endswith('.csv'):
                messages.error(request, 'THIS IS NOT A CSV FILE')
            data_set = csv_file.read().decode('UTF-8')
            mostrar = True
            lines = data_set.split("\n")
            dato = []
            # leer las lineas del csv
            for i in range(0, len(lines) - 1):
                # crear listas con el separador
                # dato.append(lines[i].split("|"))
                dato.append(json.loads(lines[i]))
            # data por limpiar
            sucia = pd.DataFrame(dato)
            # metodo para limpiar data
            frame = limpiar(sucia)
            #################################################
            # preparar los datos para predecir el abandono del curso
            pred = frame.groupby('user_id')['event_type'].value_counts().unstack().fillna(0)
            # importar lo datos MOOCs que se va a predecir el abandono
            pred.to_csv('datosPredecir.csv')
            valor, data, data2 = archivoPredict()
            dato = predModel(valor, data, data2, slug="mlp")
            l = dato.groupby('estado').count()['1'].to_list()
            datos = dato.to_dict('records')
            for iiii in range(2,7):
                var = os.path.exists("semana" + str(iiii) + '.csv')
                if(var==True):
                    os.remove("semana" + str(iiii) + '.csv')
            for iii in range(1, len(datos)+1):
                # localizar las actividades por semana
                semana = frame.loc[frame['time'] == iii]
                # realizar un conteo por semana y estudiante
                semana = semana.groupby('user_id')['event_type'].value_counts().unstack().fillna(0)
                # registrar los log por semana en archivos csv
                semana.to_csv("semana" + str(iii) + '.csv')
        except Exception as e:
            print(e)
    try:
        valSem[0]=(sum(list(pd.read_csv('semana1.csv').select_dtypes(np.number).sum())[1:]))
        valSem[1] = (sum(list(pd.read_csv('semana2.csv').select_dtypes(np.number).sum())[1:]))
        valSem[2] = (sum(list(pd.read_csv('semana3.csv').select_dtypes(np.number).sum())[1:]))
        valSem[3] = (sum(list(pd.read_csv('semana4.csv').select_dtypes(np.number).sum())[1:]))
        valSem[4] = (sum(list(pd.read_csv('semana5.csv').select_dtypes(np.number).sum())[1:]))
        valSem[5] = (sum(list(pd.read_csv('semana6.csv').select_dtypes(np.number).sum())[1:]))
        print(valSem)
    except:
        print('')
    return render(request, 'abandoCurso/promedio.html', {'sem1':valSem[0],'sem2':valSem[1],
                                                      'sem3': valSem[2], 'sem4':valSem[3],'sem5':valSem[4],
                                                      'sem6': valSem[5], 'mostrar': mostrar})

def semana(data2, user, select):
    if select == '0':
        val = data2.to_dict('records')
    else:
        a_dict['Suma'] = 8
        archivos = glob("semana*.csv")
        df2 = pd.DataFrame()
        # leer datos predecidos modelo
        ds2 = pd.read_csv('modeloResult.csv')
        # pbtner el estado nota
        s = ds2.loc[ds2['user_id'].isin(user)][['estado']]
        estado = s['estado'].values[0]
        # leer todos los archivos eventos semana csv
        ds = [pd.read_csv(f) for f in archivos]
        for i in range(0, len(ds)):
            valor = ds[i].loc[ds[i]['user_id'].isin([user])]
            if len(valor) > 0:
                df2 = pd.concat([df2, valor])
            else:
                d = {'user_id': user, '1': [0], '2': [0], '3': [0], '4': [0], '5': [0], '6': [0], '7': [0]}
                valor = pd.DataFrame(data=d)
                df2 = pd.concat([df2, valor])
        nuevo = []
        for i in range(0, 6):
            nuevo.append(estado)
        df2['estado'] = nuevo
        val = df2.to_dict('records')
    return val