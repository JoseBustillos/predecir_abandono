from django import forms
import csv


class NewArticleForm(forms.Form):
    # agregar opcion para visualizar en el template
    def __init__(self, *args, **kwargs):
        super(NewArticleForm, self).__init__(*args, **kwargs)
        self.fields['moocs'] = forms.ChoiceField(choices=[("1", "Educacion para alimentación saludable")])
        self.fields['semana'] = forms.ChoiceField(
            choices=[(0, 'Todo'), (1, "Semana 1"), (2, "Semana 2"), (3, "Semana 3"), (4, "Semana 4"), (5, "Semana 5"),
                     (6, "Semana 6")], label="Semanas ")


class NewArticleForm2(forms.Form):
    # agregar opcion para visualizar en el template
    def __init__(self, *args, **kwargs):
        super(NewArticleForm2, self).__init__(*args, **kwargs)
        self.fields['moocs'] = forms.ChoiceField(choices=[("1", "Educacion para alimentación saludable")])


class perceptronForm(forms.Form):
    maximas_interacciones = forms.IntegerField(required=True)
    numero_neuronas = forms.IntegerField(required=True)
    numero_capas = forms.IntegerField(
        widget=forms.NumberInput(attrs={'type': 'range', 'min': '1', 'max': '5', 'class': 'slider', 'id': 'myRange2'}))


class arbolForm(forms.Form):
    profundidad_maxima = forms.IntegerField(required=True)


class NameForm(forms.Form):
    has = []
    with open('./semana1.csv') as csv_file:
        # leer archivo csv
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        # leer lineas
        for row in csv_reader:
            if line_count == 0:
                linea = row
                line_count += 1
            # obtener id del usuario y almacenarlo en una lista
            has.append(row[0])

    # metodo para dar formato para html form usuarios
    def merge(list1, list2):
        merged_list = [(p1, p2) for idx1, p1 in enumerate(list1)
                       for idx2, p2 in enumerate(list2) if idx1 == idx2]
        return merged_list

    sa = []
    # convierte la lista de string a int, luego los ordenada para transformarlos de nuevo a string
    sa = list(map(str, sorted(list(map(int, has[1:])))))
    Seleccionar_Usuario = forms.ChoiceField(choices=merge(sa, sa))


class OptimaForm(forms.Form):
    # agregar opcion para visualizar en el template
    def __init__(self, *args, **kwargs):
        super(OptimaForm, self).__init__(*args, **kwargs)
        self.fields['Seleccionar'] = forms.ChoiceField(choices=[(0, "General"), (1, "Semana")])
