import pandas as pd

# nombre del archivo json
data = pd.read_json(r'../log-course-v1_UTPL+EFHE11+2019.json', lines=True)
# tranformar columna time en fecha
data['time'] = pd.to_datetime(data['time'])
# seleccionar solo los eventos que pasan delante de fecha inicio MOOC
data = data.loc[data['time'] > '2019-04-15']
# fecha se transforma al numero de la semana año
data['time'] = pd.to_datetime(data['time']).dt.week
# calcula las 6 semanas del curso MOOC y selecciona
lim = list(data['time'].unique())[0] + 6
# calcular el numero limite para restar y calcular semana MOOC
lim2 = list(data['time'].unique())[0] - 1
# elige solo las fechas dentro de las semanas MOOC
data = data.loc[data['time'] < lim]
# al numero de semana año la pasa a semana MOOC
data['time'] = data['time'] - lim2
# reemplaza los eventos en el numero del evento general que pertence
data = data.replace(['hide_transcript', 'load_video', 'pause_video', 'play_video', 'seek_video', 'show_transcript',
                     'speed_change_video', 'stop_video', 'video_hide_cc_menu', 'video_show_cc_menu'], 1)
data = data.replace(['edx.forum.comment.created', 'edx.forum.response.created', 'edx.forum.response.voted',
                     'edx.forum.searched', 'edx.forum.thread.created', 'edx.forum.thread.voted'], 2)
data = data.replace(['openassessmentblock.get_peer_submission',
                     'openassessmentblock.get_submission_for_staff_grading', 'openassessmentblock.peer_assess',
                     'openassessmentblock.self_assess', 'openassessmentblock.staff_assess',
                     'openassessmentblock.submit_feedback_on_assessments',
                     'openassessmentblock.create_submission', 'openassessmentblock.save_submission',
                     'openassessment.student_training_assess_example', 'openassessment.upload_file'], 3)
data = data.replace(['page_close', 'seq_goto', 'seq_next', 'seq_prev'], 4)
data = data.replace(['edx.course.enrollment.activated', 'edx.course.enrollment.deactivated',
                     'edx.course.enrollment.mode_changed', 'edx.course.enrollment.upgrade.clicked',
                     'edx.course.enrollment.upgrade.succeeded'], 5)
data = data.replace(['book', 'textbook.pdf.thumbnails.toggled', 'textbook.pdf.thumbnail.navigated',
                     'textbook.pdf.outline.toggled', 'textbook.pdf.chapter.navigated',
                     'textbook.pdf.page.navigated', 'textbook.pdf.zoom.buttons.changed',
                     'textbook.pdf.zoom.menu.changed', 'textbook.pdf.display.scaled',
                     'textbook.pdf.page.scrolled', 'textbook.pdf.search.executed',
                     'textbook.pdf.search.navigatednext', 'textbook.pdf.search.highlight.toggled',
                     'textbook.pdf.searchcasesensitivity.toggled'], 6)
data = data.replace(['edx.problem.hint.demandhint_displayed', 'edx.problem.hint.feedback_displayed',
                     'problem_check', 'problem_check_fail', 'problem_graded', 'problem_rescore',
                     'problem_rescore_fail', 'problem_reset', 'problem_save', 'problem_show', 'reset_problem',
                     'reset_problem_fail', 'save_problem_fail', 'save_problem_success', 'showanswer'], 7)
# selecciona solo los eventos que en realidad se soncideran eventos para MOOOC
data = data.loc[data['event_type'].isin([1, 2, 3, 4, 5, 6, 7])]
# elimina valores nulos de la columna time
data = data.dropna(subset=['time'])
# la columna context existe un json tomando ese valor
dt = pd.io.json.json_normalize(data['context'])
# del json anterior se toma solo el user_id y se le agrega al dataframe que se va a exportar
data['user_id'] = dt['user_id'].values
# elimina campos nulos de la columna user_id
data = data[data['username'].map(len) > 0]
# exporta dataframe
data.to_csv('eventMOOC.csv', sep='|')